package fr.epita.tp8;

import fr.epita.tp8.domaine.Voiture;
import fr.epita.tp8.infra.DaoFactory;
import fr.epita.tp8.infra.DaoVoiture;


public class App {

	public static void main(String[] args) {
		Voiture v=new Voiture();
		
		v.setCouleur("rouge");
		v.setImmatricultation("yyyy");
		v.setModele("twingo");
		
		
		//DaoVoiture dao=new DaoVoitureImpl
		DaoVoiture dao=DaoFactory.createVoitureDao();
		dao.create(v);

	}

}
