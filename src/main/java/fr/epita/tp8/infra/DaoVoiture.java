package fr.epita.tp8.infra;

import fr.epita.tp8.domaine.Voiture;

public interface DaoVoiture {
	
	void create(Voiture v);

}
