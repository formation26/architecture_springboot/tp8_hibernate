package fr.epita.tp8.infra;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

import fr.epita.tp8.domaine.Voiture;

public class DaoVoitureImpl implements DaoVoiture {

	@Override
	public void create(Voiture v) {
		
		SessionFactory sessionFactory=DaoFactory.getSessionFactory();
	
		Session session =sessionFactory.openSession();
		session.beginTransaction();
		session.save(v);
		session.getTransaction().commit();
		session.close();


	}

}
