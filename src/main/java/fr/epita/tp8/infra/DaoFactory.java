package fr.epita.tp8.infra;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class DaoFactory {

	private static SessionFactory sessionFactory;

	public static DaoVoiture createVoitureDao() {
		return new DaoVoitureImpl();
	}

	public static SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			sessionFactory = new Configuration().configure("hibernate.cfg.xml").buildSessionFactory();
		}
		return sessionFactory;
	}
}
